<p align="center">
	<strong>更简单的jmeter（非GUI模式）</strong>
</p>

#### 更新说明
> <font size=3>master分支将作为稳定版本发布，develop分支将会不定期进行更新，欢迎大家提供宝贵意见</font>

---

#### 项目概述
> <font size=3>基于jmeter二次封装，降低jmeter（非GUI模式）的使用门槛。</font>

---

#### 当前版本
> <font size=3>v1.0.0</font>

---

#### jmeter对应版本
> <font size=3>5.5</font>

---

#### 使用环境
> <font size=3>jdk 1.8+</font>

---

#### 项目特性

- ##### 创建测试计划
- ##### 执行测试计划
- ##### 导入测试计划
- ##### 导出测试计划

---

#### 安装教程
```cmd
mvn clean install
```

---

#### maven坐标
```maven
<dependency>
    <groupId>wiki.xsx</groupId>
    <artifactId>easy-jmeter</artifactId>
    <version>1.0.0</version>
</dependency>
```

---

#### 快速体验

```java
// 创建测试计划
JmeterTestPlan testPlan = JmeterFactory.createTestPlan();
// 创建测试线程组
JmeterDefaultThreadGroup threadGroup = JmeterFactory.createThreadGroup();
// 创建测试样本
JmeterDefaultHttpSampler httpSampler = JmeterFactory.createHttpSampler().setDomain("www.baidu.com");
// 添加样本
threadGroup.addSampler(httpSampler);
// 添加线程组
testPlan.setThreadGroup(threadGroup);

// 执行测试计划
JmeterEngineStarter.run(testPlan);
// 导出测试计划
JmeterEngineStarter.export(testPlan, "E:\\jmeter\\test\\test.jmx");
// 导入测试计划
HashTree tree = JmeterEngineStarter.load("E:\\jmeter\\test\\test.jmx");
```