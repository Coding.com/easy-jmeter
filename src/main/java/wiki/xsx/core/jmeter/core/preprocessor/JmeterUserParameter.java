package wiki.xsx.core.jmeter.core.preprocessor;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 用户参数
 *
 * @author xsx
 * @date 2022/9/2
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
@Data
@Accessors(chain = true)
public class JmeterUserParameter {

    /**
     * 参数名称
     */
    private String name;
    /**
     * 参数值列表
     */
    private List<Object> values = new ArrayList<>(10);

    /**
     * 无参构造
     */
    private JmeterUserParameter() {
    }

    /**
     * 获取用户参数实例
     *
     * @return 返回用户参数实例
     */
    public static JmeterUserParameter getInstance() {
        return new JmeterUserParameter();
    }

    /**
     * 初始化参数值容量
     *
     * @param initialCapacity 初始容量
     * @return 返回用户参数
     */
    public JmeterUserParameter initialValueCapacity(int initialCapacity) {
        this.values = new ArrayList<>(initialCapacity);
        return this;
    }

    /**
     * 添加参数值
     *
     * @param values 参数值
     * @return 返回用户参数
     */
    public JmeterUserParameter addValue(Object... values) {
        // 如果参数值列表未初始化，则初始化参数值列表
        if (this.values == null) {
            // 初始化参数值列表
            this.values = new ArrayList<>(10);
        }
        // 添加参数值列表
        Collections.addAll(this.values, values);
        return this;
    }
}
