package wiki.xsx.core.jmeter.core;

import wiki.xsx.core.jmeter.core.assertion.JmeterDefaultJsonPathAssertion;
import wiki.xsx.core.jmeter.core.assertion.JmeterDefaultResponseAssertion;
import wiki.xsx.core.jmeter.core.controller.JmeterDefaultLoopController;
import wiki.xsx.core.jmeter.core.postprocessor.JmeterDefaultJsonPostProcessor;
import wiki.xsx.core.jmeter.core.postprocessor.JmeterJsonVariable;
import wiki.xsx.core.jmeter.core.preprocessor.JmeterDefaultUserParametersPreProcessor;
import wiki.xsx.core.jmeter.core.preprocessor.JmeterUserParameter;
import wiki.xsx.core.jmeter.core.result.JmeterDefaultResultCollector;
import wiki.xsx.core.jmeter.core.sampler.JmeterDefaultHttpSamplerConfig;
import wiki.xsx.core.jmeter.core.sampler.JmeterDefaultHttpSampler;
import wiki.xsx.core.jmeter.core.threadgroup.JmeterDefaultThreadGroup;

/**
 * jmeter工厂
 *
 * @author xsx
 * @date 2022/8/23
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public class JmeterFactory {

    /**
     * 创建测试计划
     *
     * @return 返回测试计划
     */
    public static JmeterTestPlan createTestPlan() {
        return JmeterTestPlan.getInstance();
    }

    /**
     * 创建线程组
     *
     * @return 返回线程组
     */
    public static JmeterDefaultThreadGroup createThreadGroup() {
        return JmeterDefaultThreadGroup.getInstance();
    }

    /**
     * 创建循环控制器
     *
     * @return 返回循环控制器
     */
    public static JmeterDefaultLoopController createLoopController() {
        return JmeterDefaultLoopController.getInstance();
    }

    /**
     * 创建http采样器
     *
     * @return 返回http采样器
     */
    public static JmeterDefaultHttpSampler createHttpSampler() {
        return JmeterDefaultHttpSampler.getInstance();
    }

    /**
     * 创建结果收集器
     *
     * @return 返回结果收集器
     */
    public static JmeterDefaultResultCollector createResultCollector() {
        return JmeterDefaultResultCollector.getInstance();
    }

    /**
     * 创建jsonPath断言
     *
     * @return 返回jsonPath断言
     */
    public static JmeterDefaultJsonPathAssertion createJsonPathAssertion() {
        return JmeterDefaultJsonPathAssertion.getInstance();
    }

    /**
     * 创建响应断言
     *
     * @return 返回响应断言
     */
    public static JmeterDefaultResponseAssertion createResponseAssertion() {
        return JmeterDefaultResponseAssertion.getInstance();
    }

    /**
     * 创建用户参数前置处理器
     *
     * @return 返回用户参数前置处理器
     */
    public static JmeterDefaultUserParametersPreProcessor createUserParametersPreProcessor() {
        return JmeterDefaultUserParametersPreProcessor.getInstance();
    }

    /**
     * 创建json后置处理器
     *
     * @return 返回json后置处理器
     */
    public static JmeterDefaultJsonPostProcessor createJsonPostProcessor() {
        return JmeterDefaultJsonPostProcessor.getInstance();
    }


    /**
     * 创建采样器配置
     *
     * @return 返回采样器配置
     */
    public static JmeterDefaultHttpSamplerConfig createHttpSamplerConfig() {
        return JmeterDefaultHttpSamplerConfig.getInstance();
    }

    /**
     * 创建json变量
     *
     * @return 返回json变量
     */
    public static JmeterJsonVariable createJsonVariable() {
        return JmeterJsonVariable.getInstance();
    }

    /**
     * 创建用户参数
     *
     * @return 返回用户参数
     */
    public static JmeterUserParameter createUserParameter() {
        return JmeterUserParameter.getInstance();
    }
}
