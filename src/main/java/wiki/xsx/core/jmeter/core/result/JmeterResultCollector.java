package wiki.xsx.core.jmeter.core.result;

import org.apache.jmeter.visualizers.StatVisualizer;
import org.apache.jmeter.visualizers.SummaryReport;

import java.util.List;

/**
 * 结果收集器
 *
 * @author xsx
 * @date 2022/8/24
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public interface JmeterResultCollector {

    /**
     * 聚合报告GUI类名称
     */
    String STAT_VISUALIZER_GUI_CLASS_NAME = StatVisualizer.class.getName();
    /**
     * 汇总报告GUI类名称
     */
    String SUMMARY_REPORT_GUI_CLASS_NAME = SummaryReport.class.getName();

    /**
     * 获取结果收集器配置列表
     *
     * @return 返回结果收集器配置列表
     */
    List<JmeterResultCollectorConfig> getConfigs();
}
