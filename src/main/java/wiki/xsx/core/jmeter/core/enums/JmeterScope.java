package wiki.xsx.core.jmeter.core.enums;

/**
 * 范围
 *
 * @author xsx
 * @date 2022/8/30
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public enum JmeterScope {
    /**
     * 全部
     */
    ALL,
    /**
     * 仅主样本
     */
    ONLY_MAIN_SAMPLE,
    /**
     * 仅子样本
     */
    ONLY_SUB_SAMPLE,
    /**
     * 变量
     */
    VARIABLE;
}
