package wiki.xsx.core.jmeter.core.enums;

/**
 * http方法
 *
 * @author xsx
 * @date 2022/8/31
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public enum JmeterHttpMethod {

    /**
     * OPTIONS请求
     */
    OPTIONS,
    /**
     * HEAD请求
     */
    HEAD,
    /**
     * TRACE请求
     */
    TRACE,
    /**
     * GET请求
     */
    GET,
    /**
     * POST请求
     */
    POST,
    /**
     * PUT请求
     */
    PUT,
    /**
     * DELETE请求
     */
    DELETE,
    /**
     * CONNECT请求
     */
    CONNECT;
}
