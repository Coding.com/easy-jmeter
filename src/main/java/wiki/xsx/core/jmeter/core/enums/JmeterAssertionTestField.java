package wiki.xsx.core.jmeter.core.enums;

/**
 * 断言字段
 *
 * @author xsx
 * @date 2022/8/30
 * @since 1.8
 * <p>
 * Copyright (c) 2022 xsx All Rights Reserved.
 * easy-jmeter is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * </p>
 */
public enum JmeterAssertionTestField {
    /**
     * 响应头
     */
    RESPONSE_HEADERS,
    /**
     * 响应码
     */
    RESPONSE_CODE,
    /**
     * 响应数据
     */
    RESPONSE_DATA,
    /**
     * 响应文档
     */
    RESPONSE_DATA_AS_DOCUMENT,
    /**
     * 响应信息
     */
    RESPONSE_MESSAGE,
    /**
     * 请求头
     */
    REQUEST_HEADERS,
    /**
     * 请求路径
     */
    REQUEST_URL,
    /**
     * 请求数据
     */
    REQUEST_DATA;
}
