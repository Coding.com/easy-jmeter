import wiki.xsx.core.jmeter.core.JmeterEngineStarter;
import wiki.xsx.core.jmeter.core.JmeterFactory;
import wiki.xsx.core.jmeter.core.JmeterTestPlan;
import wiki.xsx.core.jmeter.core.enums.JmeterAssertionMatchMode;
import wiki.xsx.core.jmeter.core.enums.JmeterAssertionTestField;
import wiki.xsx.core.jmeter.core.enums.JmeterHttpMethod;
import wiki.xsx.core.jmeter.core.enums.JmeterHttpProtocol;
import wiki.xsx.core.jmeter.core.sampler.JmeterDefaultHttpSampler;
import wiki.xsx.core.jmeter.core.threadgroup.JmeterDefaultThreadGroup;
import org.junit.Test;

import java.io.File;
import java.util.Collections;

/**
 * @author xsx
 * @date 2022/8/22
 * @since 1.8
 */
public class JmeterTest {

    @Test
    public void test() {
        // 创建测试计划
        JmeterTestPlan testPlan = JmeterFactory.createTestPlan();
        // 创建测试线程组
        JmeterDefaultThreadGroup threadGroup = JmeterFactory.createThreadGroup();
        // 创建测试样本
        JmeterDefaultHttpSampler httpSampler = JmeterFactory.createHttpSampler().setDomain("www.baidu.com");
        // 添加样本
        threadGroup.addSampler(httpSampler);
        // 添加线程组
        testPlan.setThreadGroup(threadGroup);
        // 启动测试
        JmeterEngineStarter.run(testPlan);
        // 导出测试
        JmeterEngineStarter.export(testPlan, "E:\\jmeter\\test\\test.jmx");
    }

    @Test
    public void testRun() {
        JmeterEngineStarter.run(this.getJmeterTestPlan());
    }

    @Test
    public void testExport() {
        JmeterEngineStarter.export(
                this.getJmeterTestPlan(),
                "E:\\jmeter\\test\\test.jmx"
        );
    }

    @Test
    public void testLoad() {
        JmeterEngineStarter.load(new File("E:\\workspace\\test\\jmeter-server\\target\\test.jmx"));
    }

    private JmeterTestPlan getJmeterTestPlan() {
        return JmeterFactory.createTestPlan()
                .setName("我的测试计划")
                .setComment("测试string接口与test接口")
                .setVariables(Collections.singletonMap("test", "xsx"))
                // .setClassPathList(Arrays.asList("E:\\workspace\\", "E:\\workspace\\test"))
                .setThreadGroup(
                        JmeterFactory.createThreadGroup()
                                .setName("测试组")
                                .setLoopController(
                                        JmeterFactory.createLoopController().setName("测试循环控制器")
                                )
                                .addSampler(
                                        JmeterFactory.createHttpSampler()
                                                        .setDomain("localhost")
                                                        .setPort(8083)
                                                        .setPath("/id/string")
                                                        .setProtocol(JmeterHttpProtocol.HTTP.name())
                                                        .addAssertion(
                                                                JmeterFactory.createJsonPathAssertion().setJsonPath("$.data"),
                                                                JmeterFactory.createResponseAssertion()
                                                                        .setTestField(JmeterAssertionTestField.RESPONSE_CODE)
                                                                        .setMatchMode(JmeterAssertionMatchMode.EQUALS)
                                                                        .setMatchContents(Collections.singletonList("200"))
                                                        )
                                                        .addPostProcessor(
                                                                JmeterFactory.createJsonPostProcessor()
                                                                        .addJsonVariable(
                                                                                JmeterFactory.createJsonVariable()
                                                                                        .setVariableName("test")
                                                                                        .setJsonPath("$.data")
                                                                                        .setMatchNo(1)
                                                                        )
                                                        ),
                                        JmeterFactory.createHttpSampler()
                                                        .setDomain("localhost")
                                                        .setPort(8083)
                                                        .setPath("/id/test")
                                                        .setProtocol(JmeterHttpProtocol.HTTP.name())
                                                        .setMethod(JmeterHttpMethod.POST.name())
                                                        .setRequestHeaders(
                                                                Collections.singletonMap("Content-Type", "application/json")
                                                        )
                                                        .setIsRequestBodyParameter(Boolean.TRUE)
                                                        .setParameters(Collections.singletonMap("id", "{\"id\":\"${test}\"}"))
                                        )
                                .setResultCollector(
                                        JmeterFactory.createResultCollector()
                                                .setSaveOutputDir("E:\\workspace\\test\\jmeter-server\\target")
                                                .enableSummaryReport(null, null)
                                )
                );
    }
}
